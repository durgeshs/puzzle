<?php
set_time_limit(0);
ini_set('memory_limit','2048M');

/** class Puzzle have wine function that calculate aggregate wine bottles sold  */

class Puzzle{

	public $filename;

	function __construct($file){
		$this->filename = $file;
	}

	/** function Wine have person and wine data. output have all results */

	public function Wine(){

		$wineWishlist	= array();
		$wineList 		= array();
		$finalList 		= array();

		$wineSold 		= 0;
		$file 			= fopen($this->filename,"r");

		while (($line = fgets($file)) !== false) {

			$data = explode("\t", $line);

			$name = trim($data[0]);
			$wine = trim($data[1]);

			if(!array_key_exists($wine, $wineWishlist)){
				$wineWishlist[$wine] = [];
			}
			$wineWishlist[$wine][] = $name;
			$wineList[] = $wine;
		}
		fclose($file);
		$wineList = array_unique($wineList);
		foreach ($wineList as $key => $wine) {
			$maxSize = count($wine);
			$counter = 0;

			while($counter<10){
				$i = intval(floatval(rand()/(float)getrandmax()) * $maxSize);
				$person = $wineWishlist[$wine][$i];
				if(!array_key_exists($person, $finalList)){
					$finalList[$person] = [];
				}
				if(count($finalList[$person])<3){
					$finalList[$person][] = $wine;
					$wineSold++;
					break;
				}
				$counter++;
			}
		}

		$fh = fopen("output.txt", "w");
		fwrite($fh, "Aggregate wine bottles sold in  : ".$wineSold."\n");
		foreach (array_keys($finalList) as $key => $person) {
			foreach ($finalList[$person] as $key => $wine) {
				fwrite($fh, $person." ".$wine."\n");
			}
		}
		fclose($fh);
	}
}

$winefile = "person_wine_3.txt";

$puzzle = new Puzzle($winefile);
$puzzle->Wine();

?>